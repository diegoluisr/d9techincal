<?php

namespace Drupal\tmdb\Command;

use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\tmdb\Batch\ImportBatch;
use Drush\Commands\DrushCommands;
use Symfony\Component\Console\Output\NullOutput;

/**
 * A drush command file.
 *
 * @package Drupal\b2c\Command
 */
class TmdbImportCommand extends DrushCommands {

  use StringTranslationTrait;

  /**
   * Drush command that displays the given text.
   *
   * @param int $page_number
   *   Argument with message to be displayed.
   * @param int $pages
   *   Argument with message to be displayed.
   * @param int $actors
   *   Argument with message to be displayed.
   *
   * @command tmdb:import
   * @option update-actors
   *   Update actors data.
   * @option show-vars
   *   Show params and options.
   * @option is-popular
   *   Is popular.
   * @aliases tim
   * @usage tmdb:import --show-vars --update-actors --is-popular page_number pages actors
   */
  public function import(
    $page_number = 1,
    $pages = 1,
    $actors = 1,
    $options = [
      'update-actors' => FALSE,
      'is-popular' => FALSE,
      'show-vars' => FALSE,
    ]
  ) {

    $values = [
      'source' => boolval($options['is-popular']) ?? FALSE,
      'page' => intval($page_number) > 0 ? intval($page_number) : 1,
      'total_pages' => intval($pages) > 0 ? intval($pages) : 1,
      'update_actor' => boolval($options['update-actors']),
      'num_actors' => intval($actors) > 0 ? intval($actors) : 1,
    ];

    if ($options['show-vars'] === TRUE) {
      foreach ($options as $key => $option) {
        $this->output()->writeln($key . ': ' . $option);
      }
    }

    ImportBatch::init($values, TRUE);
  }

}
