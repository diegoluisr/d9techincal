<?php

namespace Drupal\tmdb\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form to configure batch options.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tmdb_batch_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return [
      'tmdb_batch.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('tmdb_batch.settings');

    $form['app_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('TMDB APP KEY'),
      '#default_value' => $config->get('app_key'),
      '#size' => 60,
      '#maxlength' => 255,
      '#required' => TRUE,
    ];

    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable('tmdb_batch.settings')
      ->set('app_key', $form_state->getValue('app_key'))
      ->save();

    // Send Telegram message. Ignoring Dependency Injection at will.
    // \Drupal::service('tis.telegram')->send(
    //   $this->t('@name @lastname has changed The Movie Database config API_KEY.', [
    //     '@name' => 'John',
    //     '@lastname' => 'Doe',
    //   ])
    // );
    parent::submitForm($form, $form_state);
  }

}
