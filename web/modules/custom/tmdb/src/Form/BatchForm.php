<?php

namespace Drupal\tmdb\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\tmdb\Batch\ImportBatch;

/**
 * Form to process batch option.
 */
class BatchForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tmdb_batch_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return [
      'tmdb.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['source'] = [
      '#type' => 'select',
      '#title' => $this->t('Movie source'),
      '#options' => [
        'popular' => $this->t('Popular'),
        'upcoming' => $this->t('Upcoming'),
      ],
      '#description' => $this->t('Select the movie source'),
    ];

    $form['num_actors'] = [
      '#type' => 'select',
      '#title' => $this->t('# Actors'),
      '#options' => [
        '1' => 1,
        '2' => 2,
        '3' => 3,
      ],
      '#default_value' => '1',
      '#description' => $this->t('Select maximun quantity of actors to import (Lower values reduce API consumption)'),
    ];

    $form['update_actor'] = [
      '#type' => 'checkbox',
      '#default_value' => FALSE,
      '#title' => $this->t('Update actor information'),
      '#description' => $this->t('Select if you want to update actors information (Uncheck reduce API consumption)'),
    ];

    $form['page'] = [
      '#type' => 'number',
      '#title' => $this->t('Page'),
      '#default_value' => 1,
      '#min' => 1,
      '#description' => $this->t('Select the page to import'),
    ];

    $form['total_pages'] = [
      '#type' => 'select',
      '#title' => $this->t('# Pages'),
      '#options' => [
        '1' => 1,
        '2' => 2,
        '3' => 3,
      ],
      '#default_value' => '1',
      '#description' => $this->t('Select if you want to import multiple pages (Lower values reduce API consumption)'),
    ];

    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    ImportBatch::init($values);
  }

}
