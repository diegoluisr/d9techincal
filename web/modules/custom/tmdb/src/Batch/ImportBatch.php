<?php

namespace Drupal\tmdb\Batch;

use Drupal\Core\Render\Markup;

/**
 * Class to process content bulk export.
 */
class ImportBatch {

  /**
   * Manage batch tasks for export.
   *
   * @param array $options
   *   An array with options.
   * @param bool $cli
   *   A string with the api_key value.
   */
  public static function init(array $options, bool $cli = FALSE) {

    /** @var \Drupal\tmdb\Service\TmdbImportService $tmdbImportService */
    $tmdbImportService = \Drupal::service('tmdb.import_service');

    $batch = $tmdbImportService->createOperations(
      $options,
      [get_called_class(), 'processMovie'],
      [get_called_class(), 'finishCallback']
    );

    batch_set($batch);
    if ($cli === TRUE) {
      drush_backend_batch_process();
    }
  }

  /**
   * Batch process step to import single movie information.
   */
  public static function processMovie($options, $data, &$context) {
    /** @var \Drupal\tmdb\Service\TmdbImportService $tmdbImportService */
    $tmdbImportService = \Drupal::service('tmdb.import_service');

    $tmdbImportService->processMovie($options, $data, $context);
  }

  /**
   * Implements callback_batch_finished().
   */
  public static function finishCallback($success, $results, $operations) {
    if ($success) {
      $message = t('Import process finished.');

      if (!empty($results['log']) && count($results['log'] > 0)) {
        $message .= t('<br />log: @log.',
          [
            '@log' => \Drupal::service('serialization.json')->encode($results['log']),
          ]
        );
      }

      \Drupal::service('messenger')
        ->addStatus(Markup::create($message));
    }
    else {
      // An error occurred.
      // $operations contains the operations that remained unprocessed.
      $error_operation = reset($operations);
      $message = t('An error occurred while processing %error_operation with arguments: @arguments',
        [
          '%error_operation' => $error_operation[0],
          '@arguments' => print_r($error_operation[1], TRUE),
        ]
      );
      \Drupal::service('messenger')->addError($message);
    }
  }

}
