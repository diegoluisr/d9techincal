<?php

namespace Drupal\tmdb\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\State\State;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\field\Entity\FieldStorageConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Tmdb\ApiToken;
use Tmdb\Client;
use Tmdb\Repository\GenreRepository;
use Tmdb\Repository\MovieRepository;
use Tmdb\Repository\PeopleRepository;

/**
 * The DoStuff service. Does a bunch of stuff.
 */
class TmdbImportService {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * The state.
   *
   * @var \Drupal\Core\State
   */
  protected $state;

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * B2cHelper constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The file storage backend.
   * @param \Drupal\Core\Database\Connection $database
   *   The database.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger.
   * @param \Drupal\Core\State $state
   *   The state.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    Connection $database,
    Messenger $messenger,
    State $state,
    ConfigFactoryInterface $config
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->database = $database;
    $this->messenger = $messenger;
    $this->state = $state;
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->get('messenger'),
      $container->get('state'),
      $container->get('config.factory')
    );
  }

  /**
   * Singleton function to get client instance.
   */
  public static function clientInstance($app_key) {
    static $client = NULL;
    if ($client === NULL) {
      $token = new ApiToken($app_key);
      $client = new Client($token,
        [
          'cache' => [
            'enabled' => TRUE,
          ],
        ]
      );
    }
    return $client;
  }

  /**
   * Create operations set.
   */
  public function createOperations($options, $operation_callback, $batch_finished) {
    $config = $this->config->get('tmdb_batch.settings');
    $app_key = $config->get('app_key');

    $cardinality = 10;
    $imagesStorage = FieldStorageConfig::loadByName('node', 'field_images');
    if (is_object($imagesStorage)) {
      $cardinality = $imagesStorage->getCardinality();
      if ($cardinality == -1) {
        $cardinality = 10;
      }
    }

    $this->state->set('actor_images_cardinality', $cardinality);

    $popular = boolval($options['popular'] ?? FALSE);
    $start_page = $options['page'] ?? 1;
    $max_pages = intval($options['total_pages'] ?? 1);
    $update_actor = boolval($options['update_actor'] ?? FALSE);
    $num_actors = intval($options['num_actors'] ?? 1);

    $operations = [];

    $client = self::clientInstance($app_key);
    $repository = new MovieRepository($client);

    for ($page = $start_page; $page < ($start_page + $max_pages); $page++) {

      $collection = $this->getMovieCollection($repository, $page, $popular);
      $movies = $collection->getAll();
      foreach ($movies as $movie) {
        $data = $this->getMovieBasicData($movie);
        $operations[] = [
          $operation_callback,
          [
            [
              'update_actor' => $update_actor,
              'num_actors' => $num_actors,
            ],
            $data,
          ],
        ];
      }
    }

    $batch = [
      'title' => $this->t('Importing'),
      'operations' => $operations,
      'finished' => $batch_finished,
    ];

    return $batch;
  }

  /**
   * Batch process step to import single movie information.
   */
  public function processMovie($options, $data, &$context) {
    $config = $this->config->get('tmdb_batch.settings');
    $app_key = $config->get('app_key');

    $context['message'] = 'Importing: ' . $data['title'];

    $img_base_path = 'https://image.tmdb.org/t/p/w300_and_h450_bestv2';

    if (empty($context['results']['log'])) {
      $context['results']['log'] = [];
    }

    // Movie fill step-by-step.
    $movie = $this->getContent('movie', $data['id'], $data['title']);

    if (!empty($movie)) {
      $movie->save();
      $movie->set('field_release_date', $data['releaseDate']);
      $movie->set('field_year', $data['releaseYear']);
      $movie->set('field_original_language', $data['originalLang']);

      $client = self::clientInstance($app_key);
      /** @var \Tmdb\Repository\MovieRepository $api_movie_repository */
      $api_movie_repository = new MovieRepository($client);
      /** @var \Tmdb\Model\Movie $api_movie */
      $api_movie = $api_movie_repository->load($data['id']);

      if (!is_null($api_movie)) {
        // Credits process start.
        $api_credits = $api_movie->getCredits();
        if (!is_null($api_credits)) {
          $api_collection_cast = $api_credits->getCast();

          if (!is_null($api_collection_cast)) {
            $cast = [];
            $actor_limit = $options['num_actors'];
            foreach ($api_collection_cast as $person) {
              // Limit added to reduce API consumption.
              if ($actor_limit > 0) {
                $actor_limit--;
              }
              else {
                break;
              }

              $actor = $this->getContent('actor', $person->getId(), $person->getName());
              if (!empty($actor)) {

                if ($actor->isNew() === TRUE || ($options['update_actor'] === TRUE)) {
                  $api_people_repository = new PeopleRepository($client);
                  $api_person = $api_people_repository->load($person->getId());

                  if (!is_null($api_person)) {
                    $birthday = ($api_person->getBirthday() instanceof \DateTime) ? $api_person->getBirthday()->format('Y-m-d') : NULL;
                    $deathday = ($api_person->getDeathday() instanceof \DateTime) ? $api_person->getDeathday()->format('Y-m-d') : NULL;

                    $actor->set('body', $api_person->getBiography());
                    $actor->set('field_birthday', $birthday);
                    $actor->set('field_deathday', $deathday);
                    $actor->set('field_birth_place', $api_person->getPlaceOfBirth());
                    $actor->set('field_popularity', intval($api_person->getPopularity()));
                    $actor->set('field_website', $api_person->getHomepage());
                    $actor->set('field_portrait', $img_base_path . $api_person->getProfilePath());

                    // Actor credits.
                    /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
                    $paragraph = $this->entityTypeManager->getStorage('paragraph');
                    $credit_paragraph = $paragraph->create(['type' => 'credit']);
                    $credit_paragraph->set('field_movie', [['target_id' => $movie->id()]]);
                    $credit_paragraph->set('field_character_name', $person->getCharacter());
                    if ($credit_paragraph->save()) {
                      $actor->set('field_movies',
                        [
                          [
                            'target_id' => $credit_paragraph->id(),
                            'target_revision_id' => $credit_paragraph->getRevisionId(),
                          ],
                        ]
                      );
                    }

                    // Actor images.
                    $api_images = $api_person->getImages();
                    $images = [];
                    $cardinality = $this->state->get('actor_images_cardinality');
                    foreach ($api_images as $value) {
                      $images[] = [
                        'uri' => $img_base_path . $value->getFilePath(),
                      ];
                      $cardinality--;
                      if ($cardinality == 0) {
                        break;
                      }
                    }
                    $actor->set('field_images', $images);
                  }
                  $actor->save();
                }

                $cast[] = ['target_id' => $actor->id()];
              }
            }
            $movie->set('field_actors', $cast);
          }
        }

        // Poster image.
        $api_poster_image = $api_movie->getPosterImage();
        if (!is_null($api_poster_image)) {
          $movie->set('field_poster', $img_base_path . $api_poster_image->getFilePath());
        }

        $movie->set('field_popularity', intval($api_movie->getPopularity()));
        $movie->set('body', $api_movie->getOverview());
      }

      // Genres process start.
      $genres = [];
      foreach ($data['genres'] as $genre_id) {
        $genre = $this->getTerm('genres', $genre_id, '', FALSE);
        if (is_null($genre)) {
          $client = self::clientInstance($app_key);
          $api_genre_repository = new GenreRepository($client);
          $api_genre = $api_genre_repository->load($genre_id);

          if (!is_null($api_genre)) {
            $genre = $this->getTerm('genres', $genre_id, $api_genre->getName());
            if (!empty($genre)) {
              $genre->save();
              $genres[] = ['target_id' => $genre->id()];
            }
          }
        }
        else {
          $genre->save();
          $genres[] = ['target_id' => $genre->id()];
        }
      }
      $movie->set('field_genres', $genres);

      // Production Companies process start.
      $api_companies = $api_movie->getProductionCompanies();
      $companies = [];
      foreach ($api_companies as $api_company) {
        $company = $this->getTerm('production_companies', $api_company->getId(), '', FALSE);
        if (is_null($company)) {

          if (!is_null($api_company)) {
            $company = $this->getTerm('production_companies', $api_company->getId(), $api_company->getName());
            if (!empty($company)) {
              $company->save();
              $companies[] = ['target_id' => $company->id()];
            }
          }
        }
        else {
          $company->save();
          $companies[] = ['target_id' => $company->id()];
        }
      }
      $movie->set('field_production_companies', $companies);

      // Videos process start.
      $api_videos = $api_movie->getVideos();
      foreach ($api_videos as $api_video) {
        $movie->set('field_trailer', $api_video->getUrl());
        break;
      }

      // Alternative titles process start.
      $api_titles = $api_movie->getAlternativeTitles();
      $titles = [];
      foreach ($api_titles as $api_title) {
        $titles[] = ['value' => $api_title->getTitle()];
      }
      $movie->set('field_other_names', $titles);

      // Similar moview process start.
      $api_similars = $api_movie->getSimilar();
      $similars = [];
      foreach ($api_similars as $api_similar) {
        $similar = $this->getContent('movie', $api_similar->getId(), '', FALSE);
        if (!is_null($similar)) {
          $similars[] = ['target_id' => $similar->id()];
        }
      }
      $movie->set('field_similar_movies', $similars);

      // Reviews.
      $api_reviews = $api_movie->getReviews();
      foreach ($api_reviews as $api_review) {
        $values = [
          'entity_type' => 'node',
          'entity_id'   => $movie->id(),
          'field_name'  => 'field_reviews',
          'uid' => 0,
          'comment_type' => 'review',
          'subject' => $api_review->getMediaTitle(),
          'comment_body' => $api_review->getContent(),
          'field_score' => rand(10, 50),
          'status' => 1,
        ];
        $comment = $this->entityTypeManager
          ->getStorage('comment')
          ->create($values);
        $comment->save();
      }

      $movie->save();
    }
  }

  /**
   * Convenience function to get information from movie.
   */
  public function getMovieBasicData($movie) {
    $genres = [];
    foreach ($movie->getGenres()->toArray() as $genre) {
      $genres[] = $genre->getId();
    }

    $companies = [];
    foreach ($movie->getProductionCompanies()->toArray() as $company) {
      $companies[] = $company->getId();
    }

    $data = [
      'id' => $movie->getId(),
      'title' => $movie->getOriginalTitle(),
      'genres' => $genres,
      'website' => $movie->getHomepage(),
      'popularity' => $movie->getPopularity(),
      'posterPath' => $movie->getPosterPath(),
      'originalLang' => $movie->getOriginalLanguage(),
      'releaseDate' => $movie->getReleaseDate()->format('Y-m-d'),
      'releaseYear' => $movie->getReleaseDate()->format('Y'),
    ];

    return $data;
  }

  /**
   * Get popular or upcoming movie list.
   */
  public static function getMovieCollection($repository, $page = 1, $popular = TRUE) {
    if ($popular === TRUE) {
      $collection = $repository->getPopular(['page' => $page]);
    }
    else {
      $collection = $repository->getUpcoming(['page' => $page]);
    }
    return $collection;
  }

  /**
   * Function to get or create a basic term.
   */
  public function getTerm($vocabulary_id, $migration_id, $name, $create = TRUE) {
    $term = NULL;

    /** @var \Drupal\taxonomy\TermStorage $termStorage */
    $termStorage = $this->entityTypeManager->getStorage('taxonomy_term');

    $id = $termStorage->getQuery()
      ->condition('vid', $vocabulary_id)
      ->condition('field_tmdb_id', $migration_id)
      ->execute();

    if (empty($id) && $create === TRUE) {
      /** @var \Drupal\taxonomy\TermInterface $term */
      $term = $termStorage->create(['vid' => $vocabulary_id]);
    }
    else {
      /** @var \Drupal\taxonomy\TermInterface $term */
      $term = $termStorage->load(reset($id));
    }

    if (!empty($term)) {
      if (!empty($name)) {
        $term->set('name', $name);
      }
      $term->set('field_tmdb_id', $migration_id);
    }

    return $term;
  }

  /**
   * Function to get or create a basic node.
   */
  public function getContent($bundle, $migration_id, $title, $create = TRUE) {
    $node = NULL;

    if ($title == NULL) {
      return $node;
    }

    /** @var \Drupal\taxonomy\NodeStorage $nodeStorage */
    $nodeStorage = $this->entityTypeManager->getStorage('node');

    $id = $nodeStorage->getQuery()
      ->condition('type', $bundle)
      ->condition('field_tmdb_id', $migration_id)
      ->execute();

    if (empty($id) && $create === TRUE) {
      $node = $nodeStorage->create(['type' => $bundle]);
    }
    else {
      $node = $nodeStorage->load(reset($id));
    }

    if (!empty($node)) {
      if (!empty($title)) {
        $node->set('title', $title);
      }
      $node->set('field_tmdb_id', $migration_id);
      $node->set('status', TRUE);
    }

    return $node;
  }

}
