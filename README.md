# Drupa9 Technical Test - The Movie Database
If you are interested on Docksal environment go to [Docksal powered Drupal 9 With Composer Installation](#docksal) section. This contains the original document from the Drupal/Docksal instance.

## Technical
The following code is a technical test implementing The Movie Database API.

### Why Docksal?
Docksal is a dockerized manager that allows a team to work in a homogeneous environment, trying to copy all possibles configuration of the production context to avoid or reduce any possible error or misconfiguration.

### Setup
Open your terminal and run the below commands

Clone the repository
```bash
$ git clone git@bitbucket.org:diegoluisr/d9techincal.git
```

Change directory
```bash
$ cd d9techincal
```

Below command will start the needed environment ready to start using Docksal as manager. DOCKSAL IS REQUIRED to complete the process properly.
```bash
$ fin up
```

Install Drupal and dependencies via `composer.lock` file.
```bash
$ fin composer install
```

Importing database dump with the state of work.
```
fin db import ./docksal/database/dump.sql
```

You could login using this drush
```bash
fin drush uli
```

Set TMDB API KEY. A temporal api key will be given in apart email for testing purposes.
```bash
# using drush
fin drush cset tmdb_batch.settings app_key '00000000000000000000000000000000' --input-format=string -y
```
You could access module config directly on Admin Panel through Configuration > System > The Movie Database or the nex path `/admin/config/tmdb`.

### Modules
This project have one custom module named "tmdb"

#### TMDB
Have all logic related to import information throug TMDB API

#### TIL (Telegram Integration Services)
This is a custom experimental module to send messages to Telegram Chatbot.

#### WACB (Whatapp Chat Bubble)
It is a module to show a block with a bubble than allow visitor to start a chat with a Whatsapp number.

URL depends on project folder name
### The Movie Database
After Docksal provisioning ends, you could access below URL. URL depends on project folder name
[http://d9technical.docksal/](http://d9technical.docksal/)

Then you could access Configuration > System > The Movie Database
If you don't have an API_KEY you must request it to [TMDB](https://developers.themoviedb.org/3/getting-started/introduction)

Then you could import movies, actors, genres and production companies using the Batch option.


### Code Quality Testing
I'm using Drupal and DrupalPractice standards for code quality assurance, this is implemented directly in VSCode editor

### Finally
Stop containers
```bash
$ fin stop
```

## Docksal powered Drupal 9 With Composer Installation
This is a sample Drupal 9 with Composer installation pre-configured for use with Docksal.

Features:

- Drupal 9 Composer Project
- `fin init` [example](.docksal/commands/init)
- Using the [default](.docksal/docksal.env#L9) Docksal LAMP stack with [image version pinning](.docksal/docksal.env#L13-L15)
- PHP and MySQL settings overrides [examples](.docksal/etc)

### Setup instructions

#### Step #1: Docksal environment setup

**This is a one time setup - skip this if you already have a working Docksal environment.**

Follow [Docksal environment setup instructions](https://docs.docksal.io/getting-started/setup/)

#### Step #2: Project setup

1. Clone this repo into your Projects directory

    ```
    git clone https://github.com/docksal/boilerplate-drupal9-composer.git drupal9
    cd drupal9
    ```

2. Initialize the site

    This will initialize local settings and install the site via drush

    ```
    fin init
    ```
   A `composer.lock` file will be generated. This file should be committed to your repository.

3. Point your browser to

    ```
    http://drupal9.docksal
    ```

When the automated install is complete the command line output will display the admin username and password.


### More automation with 'fin init'

Site provisioning can be automated using `fin init`, which calls the shell script in [.docksal/commands/init](.docksal/commands/init).
This script is meant to be modified per project. The one in this repo will give you a good starting example.

Some common tasks that can be handled by the init script (an other [custom commands](https://docs.docksal.io/fin/custom-commands/)):

- initialize local settings files for Docker Compose, Drupal, Behat, etc.
- import DB or perform a site install
- compile Sass
- run DB updates, revert features, clear caches, etc.
- enable/disable modules, update variables values


### Security notice

This repo is intended for quick start demos and includes a hardcoded value for `hash_salt` in `settings.php`.
If you are basing your project code base on this repo, make sure you regenerate and update the `hash_salt` value.
A new value can be generated with `drush ev '$hash = Drupal\Component\Utility\Crypt::randomBytesBase64(55); print $hash . "\n";'`
